% Examen C1223 -- Fonction d'une variable complexe
% [Sébastien Boisgérault][email], MINES ParisTech
% Samedi 20 janvier 2018

[email]: mailto:Sebastien.Boisgerault@mines-paristech.fr

---
license: "[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0)"
---


Problème R
================================================================================

**Préambule.** Soit $\Omega$ un ouvert de $\mathbb{C}$.
Pour tout $r>0$, on note $\Omega_r$ l'ensemble des points de
$\mathbb{C}$ dont la distance au complémentaire de $\Omega$ est supérieure à $r$:
  $$
  \Omega_r = \{z \in \mathbb{C} \; | \; d(z, \mathbb{C} \setminus \Omega) > r \}.
  $$

 1. Montrer que $\Omega_r$ est un sous-ensemble ouvert de $\Omega$ et que
    $\Omega = \cup_{r>0} \Omega_r$.

 2. Supposons $\Omega$ connexe. $\Omega_r$ est-il nécessairement connexe ?
    (Indication: considérer par exemple $\Omega = \{z \in \mathbb{C} 
     \; | \; |\mathrm{Im} \, z| < |\mathrm{Re} \, z| +1 \}$ et $r = 1$).

 3. Montrer que si $z \in \mathbb{C} \setminus \Omega_r$,
    alors il existe $w \in \mathbb{C} \setminus \Omega$ tel que
    le segment $[w, z]$ soit inclus dans
    $\mathbb{C} \setminus \Omega_r$.
    Déduire de cette propriété
    que si $\Omega$ est simplement connexe, 
    alors $\Omega_r$ est aussi simplement connexe.
    La réciproque est-elle vraie ?

 4. Montrer que si $\Omega$ est borné et simplement connexe,
    $\mathbb{C} \setminus \Omega$ est connexe.
    (Indication: supposer que $\Omega$ est borné mais que 
    $\mathbb{C} \setminus \Omega$ n'est pas connexe,
    puis introduire une dilatation adaptée de ce complémentaire).

Dans la suite, $\Omega$ est un ensemble borné ouvert de $\mathbb{C}$.
Soit $\mathcal{F}$ une classe de fonctions holomorphes
définies sur $\Omega$ (ou sur un ensemble contenant $\Omega$).
Une fonction holomorphe $f: \Omega \to \mathbb{C}$ 
admet des *approximations uniformes* parmi $\mathcal{F}$ si
  $$
  \forall \, \epsilon > 0, 
  \exists \, \hat{f} \in \mathcal{F}, \;
  \forall \, z \in \Omega, \; |f(z) - \hat{f}(z)| \leq \epsilon.
  $$

 5. Soit $f: \Omega \to \mathbb{C}$ une fonction holomorphe
    et soit $a  \in \mathbb{C} \setminus \Omega$.
    Supposons que $f$ admette des approximations
    uniformes parmi la classe de fonctions définies et holomorphes sur
    $\mathbb{C} \setminus \{a\}$.
    Montrer que si $|a|$ est assez grand, 
    $f$ admet des approximations uniformes parmi les polynômes.

 6. Montrer que pour tout ensemble ouvert borné non-vide $\Omega$ de $\mathbb{C}$,
    il existe une fonction holomorphe $f :\Omega  \to \mathbb{C}$
    qui n'admet pas d'approximation uniforme parmi les
    polynômes (Indication: considérer $z\mapsto 1/(z-a)$ pour un choix adéquat 
    de $a$).

Une fonction holomorphe $f: \Omega \to \mathbb{C}$ admet des *approximations localement uniformes* 
parmi $\mathcal{F}$ si pour tout $r>0$, sa restriction à tout $\Omega_r$ admet 
des approximations uniformes parmi $\mathcal{F}$:
  $$
  \forall \, \epsilon > 0, \forall \, r > 0, 
  \exists \, \hat{f} \in \mathcal{F}, \;
  \forall \, z \in \Omega_r, \; |f(z) - \hat{f}(z)| \leq \epsilon.
  $$

 7. Montrer que si $\Omega$ n'est pas simplement connexe, 
    il existe une fonction holomorphe $f: \Omega \to \mathbb{C}$
    qui n'admet pas d’approximations localement uniformes parmi les polynômes.
    (Indication: 
    considérer $f:z\mapsto 1/(z-a)$ pour un choix adéquat de $a$ 
    puis comparer
      $$
      \int_{\gamma} f(z) \, dz 
      \; \mbox{ et } \; 
      \int_{\gamma} \hat{f}(z) \, dz
      $$
    pour un choix adéquat d'un chemin $\gamma$ fermé rectifiable).

Dans la suite, on considèrera que $\Omega$ est simplement connexe.

Soit $f:\Omega \to \mathbb{C}$ et soit $r>0$.
On définit la fonction $\chi_r: \mathbb{C} \setminus \Omega \to \{0,1\}$
par:

  -  $\chi_r(z) = 1$ si pour tout $\epsilon > 0$, il existe une fonction
holomorphe $\hat{f}_z$ définie sur $\mathbb{C} \setminus \{z\}$
tel que $|f - \hat{f}_z| \leq \epsilon$ sur $\Omega_r$, 

  - $\chi_r(z) = 0$ sinon.

  8. Montrer que si deux points $z$ et $w$ de $\mathbb{C} \setminus \Omega$
     vérifient $\chi_r(z) = 1$ et $|w - z| < r/2$ alors $\chi_r(w) = 1$
     (Indication: commencer par prouver que l'anneau ouvert
     $A := A(w, r/2, +\infty)$ vérifie
     $A \subset \mathbb{C} \setminus \{z\}$ 
     et $\overline{\Omega_r} \subset A$).

  9. Montrer que $\chi_r$ est localement constante, puis
     montrer que si $\chi_r(a) = 1$ pour un certain $a \in \mathbb{C} \setminus \Omega$,
     alors $\chi_r(z) = 1$ pour tout $z \in \mathbb{C} \setminus \Omega$.

 10. Supposons que $f: \Omega \to \mathbb{C}$ admette des approximations localement uniformes
     parmi les fonctions holomorphes définies sur $\mathbb{C} \setminus \{a\}$ 
     pour un certain $a \in \mathbb{C} \setminus \Omega$.
     Montrer que $f$ admet des approximations localement uniformes parmi les polynômes.

 11. Soit $\hat{f}: \mathbb{C} \setminus \{a_1,\dots, a_n\}\to \mathbb{C}$ 
     une fonction holomorphe (tous les $a_k$ sont deux à deux distincts). 
     Montrer qu'il existe des fonctions holomorphes
     $\hat{f}_k: \mathbb{C} \setminus \{a_k\} \to \mathbb{C}$ pour $k=1,\dots,n$
     telles que
       $$
       \forall \, z \in \mathbb{C} \setminus \{a_1,\dots, a_n\}, \;
       \hat{f}(z) =  \hat{f}_1(z) + \dots + \hat{f}_n(z).
       $$

     Démontrer le corollaire suivant : si une fonction 
     $f: \Omega \to \mathbb{C}$ admet des approximations localement uniformes parmi 
     les fonctions holomorphes définies sur $\mathbb{C} \setminus \{a_1, \dots, a_n\}$ 
     pour $a_1, \dots, a_n \in \mathbb{C} \setminus \Omega$,
     alors $f$ admet des approximations localement uniformes parmi les polynômes.

\newpage

Problème L
================================================================================

Le *rayon* d'origine $a\in \mathbb{C}$ et de direction $u \in \mathbb{C}^*$
est la fonction[^Notation]
  $$
  \gamma: t \in \mathbb{R}_+ \mapsto a + t u.
  $$

[^Notation]: 
  La notation "$\gamma(t)$" va désigner un nombre complexe qui dépend évidemment
  de $t$, mais aussi implicitement de $a$ et de $u$. 
  On peut en général déduire les valeurs de $a$ et $u$ du contexte, 
  mais en cas d'ambiguité, n'hésitez pas à introduire une notation
  plus explicite.

Soit $f:\Omega \mapsto \mathbb{C}$ une fonction holomorphe définie sur un
ensemble ouvert $\Omega$ de $\mathbb{C}$ contenant l'image
$\gamma(\mathbb{R}_+)$ du rayon $\gamma$.
La transformée de Laplace $\mathcal{L}_{\gamma}[f]$ de $f$ 
selon $\gamma$ en $s \in \mathbb{C}$ est donnée par :
  $$
  \mathcal{L}_{\gamma}[f](s)
  =
  \int_{\mathbb{R}_+} f(\gamma(t)) e^{-\gamma(t) s} \gamma'(t) \, dt
  $$
(on considère que cette intégrale est définie quand l'intégrande est intégrable).
Cette définition généralise la définition classique de la transformée de Laplace $\mathcal{L}[f]$
puisque $\mathcal{L}_{\gamma}[f] = \mathcal{L}[f]$ quand $\gamma(t) = t$
(c'est à dire quand $a=0$ et $u=1$).

On suppose qu'il existe $\kappa > 0$ et $\sigma \in \mathbb{R}$ tels que :
  \begin{equation} \label{bound}
  \forall \, z \in \gamma(\mathbb{R}_+), \; |f(z)| \leq \kappa e^{\sigma |z|}.
  \end{equation}


 1. Montrer que si $\gamma(t) = a + t u$ et $\mu(t) = a + t(\lambda u)$
    pour $\lambda > 0$, alors
     $$
     \mathcal{L}_{\mu}[f] = \mathcal{L}_{\gamma}[f]
     $$
    (Rappel: deux fonctions sont égales si elles ont le même
     domaine de définition et prennent la même valeur sur ce domaine commun.)

 2. Caractériser géométriquement l'ensemble
      $$
      \Pi(u, \sigma)
      = 
      \left\{
      s \in \mathbb{C} 
      \; \left| \; 
      \mathrm{Re} \left( s u \right) > \sigma |u|
      \right.
      \right\}
      $$
    et montrer que $\mathcal{L}_\gamma[f]$ est définie et holomorphe sur
    $\Pi(u, \sigma)$.

 3. Soir $U$ un ensemble ouvert de $\mathbb{C}^*$.
    On suppose que la borne \eqref{bound} est valide pour tout $u \in U$
    (pour une origine donnée $a$ et des valeurs fixées de $\kappa$ et $\sigma$).
    Montrer que pour tout $s\in \mathbb{C}$, l'ensemble $U_{s}$ de directions
    $u \in U$
    telles que $s \in \Pi(u,\sigma)$ est ouvert et que la fonction
    $u \in U_s \mapsto \mathcal{L}_{\gamma}[f](s)$ est holomorphe.
    (Indication: montrer que le théorème de dérivation complexe sous le signe intégral
    est appliquable).

    
 4. Montrer que la dérivée de $\mathcal{L}_{\gamma}[f](s)$ 
    par rapport à $u$ est zéro (Indication: on pourra utiliser le résultat
    de la question 1).

La fonction exponentielle intégrale $E_1(x)$ est définie pour $x>0$ par :
      $$
      E_1(x) = \int_x^{+\infty} \frac{e^{-t}}{t}dt.
      $$

 5. Calculer la transformée de Laplace (classique) $F$ de  
      $$
      t \in \mathbb{R}_+ \to \frac{1}{t+1}
      $$
    et donner une formule pour $E_1(x)$ qui dépend de $F(x)$.
    Montrer que $E_1$ admet une unique extension holomorphe
    au demi-plan complexe droit $\{s \in \mathbb{C} \; | \; \mathrm{Re}(s) > 0\}$.
    
Dans la suite, on considère la fonction $f$ suivante :
  $$
  f: z \in \mathbb{C} \setminus \{-1\} \mapsto \frac{1}{z+1}
  $$ 
avec $a=0$, $\sigma=0$ et $U =\{u \in\mathbb{C} \; | \; \mathrm{Re}(u)>0\}.$

 6. Montrer qu'il existe un $\kappa > 0$ tel que 
    \eqref{bound} soit valide pour tout $u \in U$. 
    Caractériser géométriquement l'ensemble $U_s$; 
    montrer qu'il est non vide quand 
    $s\in \mathbb{C}\setminus \mathbb{R}_-$.

Soit $s \in \mathbb{C} \setminus \mathbb{R}_-$. On pose :
  $$
  G(s) := \mathcal{L}_{\gamma}[f](s)
  \; \mbox{ si } \;
  u \in U_s
  \; \mbox{ et } \;
  \gamma: t \in \mathbb{R}_+ \mapsto tu.
  $$
A noter que cette définition est *a priori* ambiguë puisqu'il existe
plusieurs $u \in U_s$ pour une valeur donnée de $s$.
Pour tout $u_0 \in \mathbb{C}^*$ et $u_1 \in \mathbb{C}^*$
et pour tout $\theta \in [0,1]$, on note
$$u_{\theta} = (1-\theta) u_0 + \theta u_1$$
et quand $u_{\theta} \neq 0$, on note
$\gamma_{\theta}$ le rayon d'origine $a=0$ et
de direction $u_{\theta}$.

    
 7. Soit $s \in \mathbb{C}\setminus \mathbb{R}_-$.
    Montrer que si $u_0 \in U_s$ et $u_1 \in U_s$,
    alors pour tout $\theta \in [0,1]$, $u_{\theta} \in U_s$ 
    et :
      $$
      \mathcal{L}_{\gamma_1}[f](s)
      - 
      \mathcal{L}_{\gamma_0}[f](s)
      =
      \int_0^1 \frac{d}{d\theta} 
      \mathcal{L}_{\gamma_{\theta}}[f](s) 
      \, d\theta.
      $$
    Conclure que la définition de $G$ est non-ambiguë.


 8. On recherche une nouvelle expression de la différence
    $\mathcal{L}_{\gamma_1}[f](s) - \mathcal{L}_{\gamma_0}[f](s)$ 
    pour bâtir une preuve alternative de la conclusion de la question précédente.

     A nouveau, soit $s \in \mathbb{C}\setminus \mathbb{R}_-$,
     $u_0 \in U_s$ et $u_1 \in U_s$;
     soit $r \geq 0$ et soient $\gamma_0^r$ et $\gamma_1^r$ les chemins définis par :
      $$
      \gamma_{0}^r: t \in [0,1] \mapsto \gamma_0(tr)
      \; \mbox{ et } \;
      \gamma_{1}^r: t \in [0,1] \mapsto \gamma_1(tr)
      $$
    Montrer que
      $$
      \mathcal{L}_{\gamma_1}[f](s) - \mathcal{L}_{\gamma_0}[f](s)
      =
      \lim_{r \to +\infty} \int_{\mu_r} f(z) e^{-sz} \, dz \;
      \; \mathrm{ où } \; \mu_r = (\gamma_0^r)^{\leftarrow} | (\gamma_1^r)
      $$
   Conclure à nouveau
   que la définition de $G$ est non-ambiguë
   (Indication: "refermer" le chemin $\mu_r$ et
   utiliser le théorème intégral de Cauchy).

 9. Prouver que $E_1$ admet une unique extension holomorphe sur
     $\mathbb{C} \setminus \mathbb{R}_-$.
   
